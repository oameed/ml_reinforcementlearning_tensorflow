#################################################
### DEEP REINFORCEMENT LEARNING WITH TFAGENTS ###
### PARAMETER DEFINITIONS                     ###
### by: OAMEED NOAKOASTEEN                    ###
#################################################

import os
import argparse

### DEFINE PARSER
parser=argparse.ArgumentParser() 

parser.add_argument('-net', type=str  ,                 help='NAME-OF-NETWORK-PROJECT', required=True)
parser.add_argument('-it' , type=int  , default=10000 , help='NUMBER OF ITERATIONS'                  )
parser.add_argument('-b'  , type=int  , default=64    , help='BATCH SIZE'                            )
parser.add_argument('-lr' , type=float, default=1e-3  , help='LEARNING RATE'                         )
parser.add_argument('-ec' , type=int  ,                 help='CODED ENVIRONMENT NAME' , required=True)

parser.add_argument('-csi', type=int  , default=100   , help='INITIAL COLLECT STEPS'                 )
parser.add_argument('-cs' , type=int  , default=1     , help='COLLECT STEPS PER ITERATION'           )
parser.add_argument('-bl' , type=int  , default=100000, help='REPLAY BUFFER MAX LENGTH'              )
parser.add_argument('-li' , type=int  , default=200   , help='LOG INTERVAL'                          )
parser.add_argument('-nee', type=int  , default=10    , help='NUM EVAL EPISODES'                     )
parser.add_argument('-ei' , type=int  , default=1000  , help='EVAL INTERVAL'                         )

### ENABLE FLAGS
args=parser.parse_args()

### CONSTRUCT PARAMETER STRUCTURES

PATHS =[os.path.join('..','..','experiments',args.net,'checkpoints' ),
        os.path.join('..','..','experiments',args.net,'logs'        ),
        os.path.join('..','..','experiments',args.net,'predictions' ) ]

PARAMS=[[args.net, args.it, args.b , args.lr                   ],
        [args.csi, args.cs, args.bl, args.li, args.nee, args.ei] ]

if args.ec in [0]:
 PARAMS.append(['CartPole-v0'])

PARAMS.append(['uniform_table'])

# PARAMS[0][0]: NETWORK DIRECTORY
# PARAMS[0][1]: num_iterations
# PARAMS[0][2]: batch_size
# PARAMS[0][3]: learning_rate
# PARAMS[1][0]: initial_collect_steps
# PARAMS[1][1]: collect_steps_per_iteration
# PARAMS[1][2]: replay_buffer_max_length
# PARAMS[1][3]: log_interval
# PARAMS[1][4]: num_eval_episodes
# PARAMS[1][5]: eval_interval
# PARAMS[2][0]: env_name
# PARAMS[3][0]: table_name


