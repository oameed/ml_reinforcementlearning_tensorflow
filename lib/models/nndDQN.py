#################################################
### DEEP REINFORCEMENT LEARNING WITH TFAGENTS ###
### LAYERS & MODEL DEFINITIONS                ###
### by: OAMEED NOAKOASTEEN                    ###
#################################################

import tensorflow as tf

def get_model(ENV):
 def layer_dense(num_units):
  initializer=tf.keras.initializers.VarianceScaling(scale       =2.0               , 
                                                    mode        ='fan_in'          , 
                                                    distribution='truncated_normal' )
  return tf.keras.layers.Dense(num_units                                   ,
                               activation        =tf.keras.activations.relu,
                               kernel_initializer=initializer               )
 from tf_agents.specs    import tensor_spec
 from tf_agents.networks import sequential
 action_tensor_spec=tensor_spec.from_spec(ENV.action_spec())
 num_actions       =action_tensor_spec.maximum-action_tensor_spec.minimum + 1
 layers_dense      =[layer_dense(num_units) for num_units in (100, 50)]
 q_values_layer    =tf.keras.layers.Dense(num_actions                                                                      ,
                                          activation        =None                                                          ,
                                          kernel_initializer=tf.keras.initializers.RandomUniform(minval=-0.03, maxval=0.03),
                                          bias_initializer  =tf.keras.initializers.Constant(-0.2)                           )
 return sequential.Sequential(layers_dense + [q_values_layer])


