#################################################
### DEEP REINFORCEMENT LEARNING WITH TFAGENTS ###
### RE-PLAY FUNCTION DEFINITIONS              ###
### by: OAMEED NOAKOASTEEN                    ###
#################################################

def get_replay_buffer_observer(PARAMS,agent):
 import                               reverb
 from tf_agents.specs          import tensor_spec
 from tf_agents.replay_buffers import reverb_replay_buffer
 from tf_agents.replay_buffers import reverb_utils
 table_name             =PARAMS[3][0]
 
 replay_buffer_signature=tensor_spec.from_spec    (agent.collect_data_spec)
 replay_buffer_signature=tensor_spec.add_outer_dim(replay_buffer_signature)
 table                  =reverb.Table(table_name                                  ,
                                      max_size    =PARAMS[1][2]                   ,
                                      sampler     =reverb.selectors.Uniform()     ,
                                      remover     =reverb.selectors.Fifo()        ,
                                      rate_limiter=reverb.rate_limiters.MinSize(1),
                                      signature   =replay_buffer_signature         )
 reverb_server          =reverb.Server([table])
 replay_buffer          =reverb_replay_buffer.ReverbReplayBuffer(agent.collect_data_spec      ,
                                                                 table_name     =table_name   ,
                                                                 sequence_length=2            ,
                                                                 local_server   =reverb_server )
 rb_observer            =reverb_utils.ReverbAddTrajectoryObserver(replay_buffer.py_client,
                                                                  table_name             ,
                                                                  sequence_length=2       )
 return replay_buffer, rb_observer


