#################################################
### DEEP REINFORCEMENT LEARNING WITH TFAGENTS ###
### UTILITY FUNCTION DEFINITIONS              ###
### by: OAMEED NOAKOASTEEN                    ###
#################################################

import os

def compute_avg_return(environment, policy, num_episodes=10):
 total_return     =0.0
 for _ in range(num_episodes):
  time_step       =environment.reset()
  episode_return  =0.0
  while not time_step.is_last():
   action_step    =policy.action(time_step)
   time_step      =environment.step(action_step.action)
   episode_return+=time_step.reward
   total_return  +=episode_return
 avg_return       =total_return/num_episodes
 return avg_return.numpy()[0]

def plotter(PATHS,PARAMS,returns):
 from matplotlib import pyplot as plt
 savefilename=os.path.join(PATHS[1],'returns'+'.png')
 iterations  =range(0,PARAMS[0][1]+1,PARAMS[1][5])
 plt.plot  (iterations, returns)
 plt.ylabel('Average Return'   )
 plt.xlabel('Iterations'       )
 #plt.ylim  (top=250)
 plt.savefig(savefilename, format='png')

def run_episodes_and_create_video(policy, eval_tf_env, eval_py_env, PATH, NAME):
 import imageio
 savefilename = os.path.join(PATH,NAME+'.gif')
 num_episodes = 3
 frames = []
 for _ in range(num_episodes):
  time_step = eval_tf_env.reset()
  frames.append(eval_py_env.render())
  while not time_step.is_last():
   action_step = policy.action(time_step)
   time_step = eval_tf_env.step(action_step.action)
   frames.append(eval_py_env.render())
 imageio.mimsave(savefilename, frames, format='gif', fps=60)


