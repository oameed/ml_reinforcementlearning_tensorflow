# Reinforcement Learning with [TF-Agents](https://www.tensorflow.org/agents) [\[GitHub\]](https://github.com/tensorflow/agents) / [Gym](https://github.com/openai/gym) / [Reverb](https://www.deepmind.com/open-source/reverb) [\[GitHub\]](https://github.com/deepmind/reverb)

## References

[[1].](https://www.tensorflow.org/agents/tutorials/0_intro_rl) 2021. TF-Agents Authors. _Introduction to RL and Deep Q Networks_  
[[2].](https://mitpress.mit.edu/books/reinforcement-learning-second-edition) 2018. Sutton. Barto. _Reinforcement Learning: An Introduction_. Second Edition [\[read online\]](https://web.stanford.edu/class/psych209/Readings/SuttonBartoIPRLBook2ndEd.pdf)  

[[3].](https://www.tensorflow.org/agents/overview) _TensorFlow Agents: Guide & Tutorials_  

_Architecture_

[[4].](https://www.nature.com/articles/nature14236) 2015. Mnih. Kavukcuoglu. Silver. _Human-level control through deep reinforcement learning_  
[[5].](https://arxiv.org/abs/1312.5602) 2013. Mnih. Kavukcuoglu. Silver. _Playing Atari with Deep Reinforcement Learning_  

_Useful_

[[6].](https://www.deepmind.com/learning-resources/reinforcement-learning-lecture-series-2021) 2021. Deep Mind. _Reinforcement Learning Lectures_  
[[7].](https://rubikscode.net/2021/07/13/deep-q-learning-with-python-and-tensorflow-2-0/) 2021. Rubik's Code. _Guide to Reinforcement Learning with Python and TensorFlow_  

## Acknowledgements

* The DQN used in this project is an adaptation from TensorFlow's tutorial: [_Train a Deep Q Network with TF-Agents_](https://www.tensorflow.org/agents/tutorials/1_dqn_tutorial)

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.82  T=0.03 s (290.9 files/s, 14316.7 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                           6             51             51            186
YAML                             1              0              0             83
Markdown                         1             16              0             37
Bourne Shell                     1              7              0             12
-------------------------------------------------------------------------------
SUM:                             9             74             51            318
-------------------------------------------------------------------------------
</pre>

## How to Run

1. `cd` to main project directory
2. `./run/sh/train_v11.sh`

## Experiments: v11

|     |     |
|:---:|:---:|
**Returns** | **Learned Policy**
![][fig_1] | ![][fig_2]

[fig_1]:experiments/v11/logs/returns.png
[fig_2]:experiments/v11/predictions/prediction.gif

