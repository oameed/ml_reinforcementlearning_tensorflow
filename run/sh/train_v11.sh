#! /bin/bash

export LD_LIBRARY_PATH=~/anaconda3/envs/tfag/lib

source  activate tfag
cd      run/tfagents

echo ' CREATING PROJECT DIRECTORY '                            'v11'
rm     -rf                                       ../../experiments/v11
tar    -xzf  ../../experiments/v00.tar.gz -C     ../../experiments
mv           ../../experiments/v00               ../../experiments/v11

echo ' TRAINING DQN AGENT IN CART-POLE ENVIRONMENT '
python train_dqn.py -net v11 -ec 0

echo ' DEMONSTRATING THE LEARNED POLICY'
python predict.py -net v11 -ec 0


