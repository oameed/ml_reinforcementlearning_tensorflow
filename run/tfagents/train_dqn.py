#################################################
### DEEP REINFORCEMENT LEARNING WITH TFAGENTS ###
### TRAINING DEEP Q NETWORK (DQN)             ###
### by: OAMEED NOAKOASTEEN                    ###
#################################################

import                             os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import                             sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))

import tensorflow               as  tf

from tf_agents.utils        import  common
from tf_agents.environments import (suite_gym         ,
                                    tf_py_environment  )
from tf_agents.agents.dqn   import  dqn_agent
from tf_agents.policies     import (random_tf_policy  ,
                                    py_tf_eager_policy,
                                    policy_saver       )
from tf_agents.drivers      import  py_driver

from paramd                 import  PATHS, PARAMS
from utilsd                 import (compute_avg_return           ,
                                    plotter                      ,
                                    run_episodes_and_create_video )
from nndDQN                 import  get_model
from rpld                   import  get_replay_buffer_observer

policy_dir                =os.path.join(PATHS[0],'policy')

env_train_py              =suite_gym.load    (PARAMS[2][0])
env_train                 =tf_py_environment.TFPyEnvironment(env_train_py)
env_eval_py               =suite_gym.load    (PARAMS[2][0])
env_eval                  =tf_py_environment.TFPyEnvironment(env_eval_py )
env                       =suite_gym.load    (PARAMS[2][0])
model                     =get_model         (env)
agent                     =dqn_agent.DqnAgent(env_train.time_step_spec()                                             ,
                                              env_train.action_spec()                                                ,
                                              q_network         =model                                               ,
                                              optimizer         =tf.keras.optimizers.Adam(learning_rate=PARAMS[0][3]),
                                              td_errors_loss_fn =common.element_wise_squared_loss                    ,
                                              train_step_counter=tf.Variable(0)                                       )
agent.initialize()
policy_eval               =agent.policy
policy_collect            =agent.collect_policy
policy_random             =random_tf_policy.RandomTFPolicy(env_train.time_step_spec(),
                                                           env_train.action_spec()    )
tf_policy_saver           =policy_saver.PolicySaver(agent.policy)

replay_buffer, rb_observer=get_replay_buffer_observer(PARAMS,agent)

py_driver.PyDriver(env                                                                    ,
                   py_tf_eager_policy.PyTFEagerPolicy(policy_random, use_tf_function=True),
                   [rb_observer]                                                          ,
                   max_steps=PARAMS[1][0]                                                  ).run(env_train_py.reset())

dataset                   =replay_buffer.as_dataset(num_parallel_calls=3           ,
                                                    sample_batch_size =PARAMS[0][2],
                                                    num_steps         =2            ).prefetch(3)
iterator                  =iter(dataset)

#############
### TRAIN ###
#############
agent.train               =common.function(agent.train)    # Optimize by wrapping some of the code in a graph using TF function. (Optional) 
agent.train_step_counter.assign(0)                         # Reset the train step.
                                                           # Evaluate the agent's policy once before training.
avg_return                =compute_avg_return(env_eval, agent.policy, PARAMS[1][4]) 
returns                   =[avg_return]
time_step                 =env_train_py.reset()            # Reset the environment.
                                                           # Create a driver to collect experience.
collect_driver            =py_driver.PyDriver(env                                                                           ,
                                              py_tf_eager_policy.PyTFEagerPolicy(agent.collect_policy, use_tf_function=True),
                                              [rb_observer]                                                                 ,
                                              max_steps=PARAMS[1][1]                                                         )

for _ in range(PARAMS[0][1]):
 time_step , _            = collect_driver.run(time_step)  # Collect a few steps and save to the replay buffer.
 experience, unused_info  = next(iterator)                 # Sample a batch of data from the buffer and update the agent's network.
 train_loss               = agent.train(experience).loss
 step                     = agent.train_step_counter.numpy()
 if step % PARAMS[1][3]  == 0:
  print('step = {0}: loss = {1}'.format          (step, train_loss))
 if step % PARAMS[1][5]  == 0:
  avg_return              = compute_avg_return(env_eval, agent.policy, PARAMS[1][4])
  print('step = {0}: Average Return = {1}'.format(step, avg_return))
  returns.append(avg_return)

plotter(PATHS,PARAMS,returns)

run_episodes_and_create_video(agent.policy,env_eval,env_eval_py,PATHS[1],'trained')

tf_policy_saver.save(policy_dir)


