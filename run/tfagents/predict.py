#################################################
### DEEP REINFORCEMENT LEARNING WITH TFAGENTS ###
### PREDICT                                   ###
### by: OAMEED NOAKOASTEEN                    ###
#################################################

import                                 os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import                                 sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))

import tensorflow                  as  tf
from tf_agents.environments import    (suite_gym         ,
                                       tf_py_environment  )
from tf_agents.policies     import     policy_saver

from paramd                 import     PATHS, PARAMS
from utilsd                 import     run_episodes_and_create_video

policy_dir  =os.path.join       (PATHS[0],'policy')
policy      =tf.saved_model.load(policy_dir       )

env_eval_py =suite_gym.load                   (PARAMS[2][0])
env_eval    =tf_py_environment.TFPyEnvironment(env_eval_py )

run_episodes_and_create_video(policy, env_eval, env_eval_py, PATHS[2],'prediction')


